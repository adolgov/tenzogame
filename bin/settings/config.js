{
/* Стоимость весов */
"price":1000000,

/* Параметры для разгрузки металлолома */
"metal":
{	
	/* Значения */
	"normal":17000,
	"addMin":900,
	"addMax":1800,

	/* Нормальная погрешность для ЧВ */
	"normalMin":0,
	"normalMax":20,
	"normalStep":20,

	/* Погрешность для УПВ */
	"deltaMin":20,
	"deltaMax":60,
	"deltaStep":20,

	/* Погрешность для УПВ в мороз */
	"deltaColdMin":20,
	"deltaColdMax":60,
	"deltaColdStep":20,

	/* Погрешность для УПВ в жару */
	"deltaHotMin":20,
	"deltaHotMax":60,
	"deltaHotStep":20,

	/* Стоимость килограмма */
	"multiplier":6.5
},

/* Параметры для уборки зерна */
"corn":
{
	/* Значения */
	"normal":8600,
	"addMin":-400,
	"addMax":-500,

	/* Нормальная погрешность для ЧВ */
	"normalMin":0,
	"normalMax":-20,
	"normalStep":-20,

	/* Погрешность для УПВ */
	"deltaMin":-20,
	"deltaMax":-60,
	"deltaStep":-20,

	/* Погрешность для УПВ в дождь */
	"deltaColdMin":-20,
	"deltaColdMax":-80,
	"deltaColdStep":-20,

	/* Погрешность для УПВ в жару */
	"deltaHotMin":-20,
	"deltaHotMax":-100,
	"deltaHotStep":-20,

	/* Стоимость килограмма */
	"multiplier":-5
},

/* Картинка (или флэшка) с прокручивающейся "помощью" */
"help":"content/help.swf",

/* Результат, когда весы еще ничего не заработали */
"noResults":"Посмотрите, как честные весы приносят вам прибыль"
}