package base {
	import flash.utils.setTimeout;

	import base.stuff.Harvester;
	import base.lorrys.LorryMarker;

	import config.Settings;

	import events.GameEvent;
	import events.SettingsEvent;

	import sound.SoundController;

	import tools.AnimationTool;
	import tools.Calculator;
	import tools.WeightResults;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;

	/**
	 * @author alexanderdolgov
	 */
	public class CornGame extends Sprite {
		private const KAMAZ_WEIGHT : Number = 10000;	
		public var container : Sprite;
		public var money : MovieClip;
		public var stuff : MovieClip;
		public var harv : Harvester;
		// tools
		private var thecar : LorryMarker;
		private var finalLap : Boolean = false;
		private var snd : SoundController;
		// settings
		internal var settings : Settings;

		public function CornGame() {
			settings = Settings.getInstance();
			this.mouseEnabled = this.mouseChildren = false;
			if (stage) onAdded();
			else addEventListener(Event.ADDED_TO_STAGE, onAdded);
			harv = stuff['harv'] as Harvester;
		}

		private function onAdded(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			stage.addEventListener(GameEvent.PAUSE, onGamePaused);
			stage.addEventListener(GameEvent.CONTINUE, onGameContinued);
			stage.addEventListener(GameEvent.PLAY_AGAIN, onGameStarted);
			stopAnimation();
			// sounds
			snd = SoundController.getInstance();
		}

		private function onGameStarted(event : GameEvent) : void {
			if (!this.visible) return;
			settings.reset();
			this.dispatchEvent(new SettingsEvent(SettingsEvent.SETTINGS_CHANGED, true));
			setTimeout(playAnimation, 1000, true);
		}

		private function onGameContinued(event : GameEvent) : void {
			if (!this.visible) return;
			playAnimation();
		}

		private function onGamePaused(event : GameEvent) : void {
			if (!this.visible) return;
			stopAnimation();
		}

		public function stopAnimation() : void {
			AnimationTool.stopAllChilds(stuff);
			AnimationTool.stopAllChilds(container);
		}

		public function playAnimation(newGame : Boolean = false) : void {
			if (newGame) {
				for (var i : int = container.numChildren - 1; i >= 0 ; i--) {
					container.removeChild(container.getChildAt(i));
				}
				launchNewCar();
			}
			AnimationTool.playAllChilds(stuff);
			AnimationTool.playAllChilds(container);
		}

		private function onLorryWeighting(event : GameEvent) : void {
			var weightResults : WeightResults = Calculator.calculateWeight(settings.currentConfig, settings.currentWeather);
			if (settings.currentRace == 0) settings.totalResults = weightResults;
			else settings.totalResults.addResults(weightResults);
			settings.currentRace++;
			//
			var brutto : String = (Math.abs(weightResults.baseWeight) + KAMAZ_WEIGHT) + "кг";
			var cost: String = Math.abs(weightResults.baseWeight * settings.currentConfig.multiplier) + "р";
			(money['money']['weightTF'] as TextField).text = brutto + "\n" + cost;
			money.play();
			if (snd && snd.sounds) snd.playEffect(snd.sounds.buyitem);
			//
			this.dispatchEvent(new SettingsEvent(SettingsEvent.SETTINGS_CHANGED, true));
			// thecar.next
			// adding new car
			if (settings.currentRace < settings.waysAmount)
				launchNewCar(thecar.state);
			else finalLap = true;
		}

		private function launchNewCar(state : int = -1) : void {
			thecar = new LorryMarker();
			thecar.state = state + 1;
			container.addChild(thecar);
			startLorry(thecar);
		}

		private function onWayFinished(event : GameEvent) : void {
			var car : LorryMarker = event.currentTarget as LorryMarker;
			stopLorry(car);
			container.removeChild(car);
			if (finalLap) {
				finalLap = false;
				this.dispatchEvent(new GameEvent(GameEvent.GAME_COMPLETED, true));
			}
		}

		private function onLorryFilling(event : GameEvent) : void {
			harv.launch();
		}

		private function stopLorry(lorry : LorryMarker) : void {
			if (lorry.hasEventListener(GameEvent.IS_WEIGHTING)) {
				lorry.removeEventListener(GameEvent.IS_FILLING, onLorryFilling);
				lorry.removeEventListener(GameEvent.IS_WEIGHTING, onLorryWeighting);
				lorry.removeEventListener(GameEvent.WAY_FINISHED, onWayFinished);
			}
			lorry.stop();
		}

		private function startLorry(lorry : LorryMarker, fromBegin : Boolean = false) : void {
			if (!lorry.hasEventListener(GameEvent.IS_WEIGHTING)) {
				lorry.addEventListener(GameEvent.IS_WEIGHTING, onLorryWeighting);
				lorry.addEventListener(GameEvent.IS_FILLING, onLorryFilling);
				lorry.addEventListener(GameEvent.WAY_FINISHED, onWayFinished);
			}
			if (fromBegin) lorry.gotoAndPlay(1);
			else lorry.play();
			if (snd && snd.sounds) snd.playEffect(snd.sounds.engine);
		}
	}
}
