package base {
	import base.lorrys.MetalMarker;

	import config.Settings;

	import events.GameEvent;
	import events.SettingsEvent;

	import sound.SoundController;

	import tools.AnimationTool;
	import tools.Calculator;
	import tools.WeightResults;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.utils.setTimeout;

	/**
	 * @author alexanderdolgov
	 */
	public class MetalGame extends Sprite {
		private const KAMAZ_WEIGHT : int = 10000;
		public var container : Sprite;
		public var money : MovieClip;
		public var stuff : MovieClip;
		// tools
		private var thecar : MetalMarker;
		private var snd : SoundController;
		// settings
		internal var settings : Settings;
		// prev results
		private var preResults : WeightResults;

		public function MetalGame() {
			settings = Settings.getInstance();
			this.mouseEnabled = this.mouseChildren = false;
			if (stage) onAdded();
			else addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			stage.addEventListener(GameEvent.PAUSE, onGamePaused);
			stage.addEventListener(GameEvent.CONTINUE, onGameContinued);
			stage.addEventListener(GameEvent.PLAY_AGAIN, onGameStarted);
			stopAnimation();
			// sounds
			snd = SoundController.getInstance();
		}

		private function onGameStarted(event : GameEvent) : void {
			if (!this.visible) return;
			settings.reset();
			this.dispatchEvent(new SettingsEvent(SettingsEvent.SETTINGS_CHANGED, true));
			setTimeout(playAnimation, 1000, true);
		}

		private function onGameContinued(event : GameEvent) : void {
			if (!this.visible) return;
			playAnimation();
		}

		private function onGamePaused(event : GameEvent) : void {
			if (!this.visible) return;
			stopAnimation();
		}

		public function stopAnimation() : void {
			AnimationTool.stopAllChilds(stuff);
			AnimationTool.stopAllChilds(container);
		}

		public function playAnimation(newGame : Boolean = false) : void {
			if (newGame) {
				for (var i : int = container.numChildren - 1; i >= 0 ; i--) {
					container.removeChild(container.getChildAt(i));
				}
				launchNewCar();
			}
			AnimationTool.playAllChilds(stuff);
			AnimationTool.playAllChilds(container);
		}

		private function onLorryIncoming(event : GameEvent) : void {
			preResults = Calculator.calculateWeight(settings.currentConfig, settings.currentWeather);
			(money['money']['weightTF'] as TextField).text = preResults.baseWeight + KAMAZ_WEIGHT + "кг";
			money.gotoAndPlay(3);

			// playing dzing sound
			if (snd && snd.sounds) snd.playEffect(snd.sounds.buyitem);
		}

		private function onLorryWeighting(event : GameEvent) : void {
			if (settings.currentRace == 0) {
				settings.totalResults = preResults;
			} else {
				settings.totalResults.addResults(preResults);
			}
			settings.currentRace++;
			this.dispatchEvent(new SettingsEvent(SettingsEvent.SETTINGS_CHANGED, true));
			//
			(money['money']['weightTF'] as TextField).text = Math.abs(preResults.baseWeight * settings.currentConfig.multiplier) + "р";
			money.gotoAndPlay(3);
			//
			// playing dzing sound
			if (snd && snd.sounds) snd.playEffect(snd.sounds.buyitem);
		}

		private function launchNewCar(state : int = -1) : void {
			thecar = new MetalMarker();
			thecar.state = state + 1;
			container.addChild(thecar);
			startLorry(thecar);
		}

		private function onWayFinished(event : GameEvent) : void {
			var car : MetalMarker = event.currentTarget as MetalMarker;
			stopLorry(car);
			container.removeChild(car);
			if (settings.currentRace == settings.waysAmount) {
				this.dispatchEvent(new GameEvent(GameEvent.GAME_COMPLETED, true));
			}
		}

		private function stopLorry(lorry : MetalMarker) : void {
			if (lorry.hasEventListener(GameEvent.IS_WEIGHTING)) {
				lorry.removeEventListener(GameEvent.IS_FILLING, onLorryFilling);
				lorry.removeEventListener(GameEvent.CAR_INCOMING, onLorryIncoming);
				lorry.removeEventListener(GameEvent.IS_WEIGHTING, onLorryWeighting);
				lorry.removeEventListener(GameEvent.WAY_FINISHED, onWayFinished);
			}
			lorry.stop();
		}

		private function startLorry(lorry : MetalMarker, fromBegin : Boolean = false) : void {
			if (!lorry.hasEventListener(GameEvent.IS_WEIGHTING)) {
				lorry.addEventListener(GameEvent.IS_FILLING, onLorryFilling);
				lorry.addEventListener(GameEvent.CAR_INCOMING, onLorryIncoming);
				lorry.addEventListener(GameEvent.IS_WEIGHTING, onLorryWeighting);
				lorry.addEventListener(GameEvent.WAY_FINISHED, onWayFinished);
			}
			if (fromBegin) lorry.gotoAndPlay(1);
			else lorry.play();
			if (snd && snd.sounds) snd.playEffect(snd.sounds.engine);
		}

		private function onLorryFilling(event : GameEvent) : void {
			if (settings.currentRace < settings.waysAmount - 1) {
				launchNewCar(thecar.state);
				trace("not final lap, launching new car!");
			}
		}
	}
}
