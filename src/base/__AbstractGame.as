package base {
	import flash.text.TextField;

	import base.lorrys.__AbstractLorry;

	import config.Settings;

	import events.GameEvent;
	import events.SettingsEvent;

	import sound.SoundController;

	import tools.AnimationTool;
	import tools.Calculator;
	import tools.WeightResults;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.setTimeout;

	/**
	 * @author alexanderdolgov
	 */
	public class __AbstractGame extends Sprite {
		public var empty : __AbstractLorry;
		public var full : __AbstractLorry;
		public var stuff : MovieClip;
		public var money : MovieClip;
		internal var settings : Settings;
		private var snd : SoundController;

		public function __AbstractGame() {
			settings = Settings.getInstance();
			this.mouseEnabled = this.mouseChildren = false;
			if (stage) onAdded();
			else addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			stage.addEventListener(GameEvent.PAUSE, onGamePaused);
			stage.addEventListener(GameEvent.CONTINUE, onGameContinued);
			stage.addEventListener(GameEvent.PLAY_AGAIN, onGameStarted);
			stopAnimation();
			snd = SoundController.getInstance();
		}

		private function onGameStarted(event : GameEvent) : void {
			if (!this.visible) return;
			settings.reset();
			this.dispatchEvent(new SettingsEvent(SettingsEvent.SETTINGS_CHANGED, true));
			setTimeout(playAnimation, 1000, true);
			// playAnimation(true);
		}

		private function onGameContinued(event : GameEvent) : void {
			if (!this.visible) return;
			playAnimation();
		}

		private function onGamePaused(event : GameEvent) : void {
			if (!this.visible) return;
			stopAnimation();
		}

		public function stopAnimation() : void {
			stopLorry(empty);
			stopLorry(full);
			// another animation
			AnimationTool.stopAllChilds(stuff);
		}

		public function playAnimation(newGame : Boolean = false) : void {
			// 1st is going!
			startLorry(full, newGame);
			// if the second (empty) is on his way, continue by button "CONTINUE"
			if (newGame) {
				empty.gotoAndStop(1);
				stopLorry(empty);
			}
			if (empty.currentFrame > 1) startLorry(empty);
			// another animation

			AnimationTool.playAllChilds(stuff);
			if (snd && snd.sounds) snd.playEffect(snd.sounds.traffic);
		}

		/**
		 * One of the main things in the class
		 * handling weighting the cars
		 */
		private function onLorryWeighting(event : GameEvent) : void {
			var car : __AbstractLorry = event.currentTarget as __AbstractLorry;
			if (car == full) {
				// trace ('full weighted');
				// firstResult = Calculator.calculateWeight(settings.currentConfig, settings.currentWeather);
				// trace ("При взвешивании полного грузовика получили")
			} else if (car == empty) {
				// trace ('empty weighted');
				var weightResults : WeightResults = Calculator.calculateWeight(settings.currentConfig, settings.currentWeather);
				if (settings.currentRace == 0) settings.totalResults = weightResults;
				else settings.totalResults.addResults(weightResults);
				settings.currentRace++;
				this.dispatchEvent(new SettingsEvent(SettingsEvent.SETTINGS_CHANGED, true));
				setTimeout(function() : void {
					(money['money']['weightTF'] as TextField).text = weightResults.difference + " кг";
					money.play();
					if (snd && snd.sounds) snd.playEffect(snd.sounds.buyitem);
				}, settings.quickMode ? 0 : 1000);
			}

			if (snd && snd.sounds) snd.playEffect(snd.sounds.brake);
		}

		private function onWayFinished(event : GameEvent) : void {
			var car : __AbstractLorry = event.currentTarget as __AbstractLorry;
			if (car == full && Settings.getInstance().currentRace == 0) {
				// second is going!
				startLorry(empty);
			} else if (car == full && Settings.getInstance().currentRace == Settings.getInstance().waysAmount - 1) {
				stopLorry(full);
			} else if (car == empty && Settings.getInstance().currentRace == Settings.getInstance().waysAmount) {
				stopLorry(empty);
				this.dispatchEvent(new GameEvent(GameEvent.GAME_COMPLETED, true));
			}
		}

		private function stopLorry(lorry : __AbstractLorry) : void {
			if (lorry.hasEventListener(GameEvent.IS_WEIGHTING)) {
				lorry.removeEventListener(GameEvent.IS_WEIGHTING, onLorryWeighting);
				lorry.removeEventListener(GameEvent.WAY_FINISHED, onWayFinished);
			}
			lorry.stop();
		}

		private function startLorry(lorry : __AbstractLorry, fromBegin : Boolean = false) : void {
			if (!lorry.hasEventListener(GameEvent.IS_WEIGHTING)) {
				lorry.addEventListener(GameEvent.IS_WEIGHTING, onLorryWeighting);
				lorry.addEventListener(GameEvent.WAY_FINISHED, onWayFinished);
			}
			if (fromBegin) lorry.gotoAndPlay(1);
			else lorry.play();
			if (snd && snd.sounds) snd.playEffect(snd.sounds.truck);
		}
	}
}
