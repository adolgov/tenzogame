package base {
	import base.lorrys.__AbstractLorry;
	import flash.display.Sprite;

	/**
	 * @author alexanderdolgov
	 */
	public class __GameCorn extends __AbstractGame {
		public var elements : Sprite;
		
		public function __GameCorn() {
			empty = new __AbstractLorry();		
			full = new __AbstractLorry();		
		}

	}
}
