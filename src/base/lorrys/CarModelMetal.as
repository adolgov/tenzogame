package base.lorrys {
	import flash.display.Sprite;

	/**
	 * @author alexanderdolgov
	 */
	public class CarModelMetal extends Sprite {
		private const MAX_X : int = 5;
		private const MAX_Y : int = 7;
		public var sprites : Sprite;
		private var _X : int;
		private var _Y : int;
		private var _rotation : Number;

		public function CarModelMetal() {
		}

		override public function set rotation(val : Number) : void {
			_rotation = val;
			if (val > -150 && val < -30 ) Y = 0;
			else if (val == -30) Y = 1;
			else if (val > -30 && val < 30) Y = 2;
			else if (val == 30) Y = 3;
			else if (val > 30 && val < 150) Y = 4;
			else if (val == 150 || val == -210) Y = 5;
			else if ((val > 150 && val < 210) || val > -210 && val < -150) Y = 6;
			else if (val == -150) Y = 7;
		}

		public function get X() : int {
			return _X;
		}

		public function set X(val : int) : void {
			_X = (val > MAX_X) ? 0 : (val < 0) ? MAX_X : val;
			sprites.x = -_X * 96;
		}

		public function get Y() : int {
			return _Y;
		}

		public function set Y(val : int) : void {
			_Y = (val > MAX_Y) ? 0 : (val < 0) ? MAX_Y : val;
			sprites.y = -_Y * 96;
		}
	}
}
