package base.lorrys {
	import config.Settings;

	import events.GameEvent;

	import sound.SoundController;

	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 */
	public class MetalMarker extends MovieClip {
		private static const FRAME_DELTA : int = 10;
		private static const MAX_STATE : int = 5;
		//
		private var notPreWieghtedYet : Boolean = true;
		private var notWieghtedYet : Boolean = true;
		private var emptyYet : Boolean = true;
		private var isFast : Boolean = false;
		private var model : CarModelMetal;
		private var _state : int = 0;
		//
		internal var preWeightFrame : int = 300;
		internal var weightFrame : int = 650;
		internal var fillingFrame : int = 500;
		internal var isFull : Boolean = true;
		private var snd : SoundController;

		public function MetalMarker() {
			this.stop();
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			snd = SoundController.getInstance();
		}

		private function onAdded(event : Event = null) : void {
			this.removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			stage.addEventListener(GameEvent.SPEED_CHANGED, onSpeedChanged);
			isFast = Settings.getInstance().quickMode;
			model = new CarModelMetal();
			model.X = state;
			onEnterFrame(null);
			addChild(model);

			// playing sounds
			if (snd && snd.sounds) snd.playEffect(snd.sounds.truck);
		}

		private function onRemoved(event : Event) : void {
			this.removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			if (hasEventListener(Event.ENTER_FRAME)) this.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.stop();
		}

		private function onSpeedChanged(event : GameEvent) : void {
			isFast = !isFast;
		}

		override public function play() : void {
			if (!this.hasEventListener(Event.ENTER_FRAME))
				this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.play();
			if (snd && snd.sounds) snd.playCarSound(this);
		}

		override public function gotoAndPlay(frame : Object, scene : String = null) : void {
			if (!this.hasEventListener(Event.ENTER_FRAME))
				this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.gotoAndPlay(frame, scene);
			if (snd && snd.sounds) snd.playCarSound(this);
		}

		override public function stop() : void {
			if (hasEventListener(Event.ENTER_FRAME))
				this.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.stop();
			if (snd && snd.sounds) snd.stopCarSound(this);
		}

		private function onEnterFrame(event : Event) : void {
			var child : Shape = Shape(this.getChildAt(0));
			if (child) {
				if (child.visible) child.visible = false;
				var rot : Number = Math.round(child.rotation);
				model.x = child.x - 48;
				model.y = child.y - 48;
				model.rotation = rot;
			}
			if (isFast) {
				playFast();
			}
			if (currentFrame > preWeightFrame && notPreWieghtedYet) {
				dispatchEvent(new GameEvent(GameEvent.CAR_INCOMING, true));
				notPreWieghtedYet = false;
			} else if (currentFrame > fillingFrame && emptyYet) {
				state++;
				emptyYet = false;
				dispatchEvent(new GameEvent(GameEvent.IS_FILLING, true));
			} else if (currentFrame > weightFrame && notWieghtedYet ) {
				notWieghtedYet = false;
				if (snd && snd.sounds) snd.playEffect(snd.sounds.brake);
				dispatchEvent(new GameEvent(GameEvent.CAR_RETURN, true));
				dispatchEvent(new GameEvent(GameEvent.IS_WEIGHTING, true));
			} else if (currentFrame == totalFrames) {
				dispatchEvent(new GameEvent(GameEvent.WAY_FINISHED, true));
			}
		}

		private function playFast() : void {
			this.gotoAndPlay(this.currentFrame + FRAME_DELTA);
		}

		public function get state() : int {
			return _state;
		}

		public function set state(val : int) : void {
			_state = (val > MAX_STATE) ? 0 : (val < 0) ? MAX_STATE : val;
			if (model) model.X = _state;
		}
	}
}
