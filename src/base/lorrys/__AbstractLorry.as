package base.lorrys {
	import sound.SoundController;
	import events.GameEvent;

	import flash.display.MovieClip;
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 */
	public class __AbstractLorry extends MovieClip {
		private static const FRAME_DELTA : int = 10;
		//
		private var notWieghtedYet : Boolean = true;
		private var isFast : Boolean = false;
		internal var weightFrame : int = 125;
		internal var isFull : Boolean = true;
		internal var snd : SoundController;
		
		public function __AbstractLorry() {
			this.stop();
			if (stage) onAdded();
			else addEventListener(Event.ADDED_TO_STAGE, onAdded);
			snd = SoundController.getInstance();
		}

		private function onAdded(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			stage.addEventListener(GameEvent.SPEED_CHANGED, onSpeedChanged);
		}

		private function onSpeedChanged(event : GameEvent) : void {
			isFast = !isFast;
		}

		override public function play() : void {
			if (!hasEventListener(Event.ENTER_FRAME))
				this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.play();
			if (snd && snd.sounds) snd.playCarSound(this);
		}

		override public function gotoAndPlay(frame : Object, scene : String = null) : void {
			if (!hasEventListener(Event.ENTER_FRAME))
				this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.gotoAndPlay(frame, scene);
			if (snd && snd.sounds) snd.playCarSound(this);
		}

		override public function stop() : void {
			if (hasEventListener(Event.ENTER_FRAME))
				this.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.stop();
			if (snd && snd.sounds) snd.stopCarSound(this);
		}

		private function onEnterFrame(event : Event) : void {
			if (isFast) {
				playFast();
			}
			if (currentFrame > weightFrame && notWieghtedYet ) {
				notWieghtedYet = false;
				dispatchEvent(new GameEvent(isFull ? GameEvent.CAR_INCOMING : GameEvent.CAR_RETURN, true));
				dispatchEvent(new GameEvent(GameEvent.IS_WEIGHTING));
			} else if (currentFrame == totalFrames) {
				dispatchEvent(new GameEvent(GameEvent.WAY_FINISHED));
				notWieghtedYet = true;
			}
		}

		private function playFast() : void {
			this.gotoAndPlay(this.currentFrame + FRAME_DELTA);
		}
	}
}
