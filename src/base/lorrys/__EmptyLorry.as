package base.lorrys {
	import flash.display.MovieClip;


	/**
	 * @author alexanderdolgov
	 */
	public class __EmptyLorry extends __AbstractLorry {
		public var car1 : MovieClip;

		public function __EmptyLorry() {
			this.weightFrame = 125;
		}
	}
}
