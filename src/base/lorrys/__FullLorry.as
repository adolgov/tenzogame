package base.lorrys {
	import flash.display.MovieClip;

	/**
	 * @author alexanderdolgov
	 */
	public class __FullLorry extends __AbstractLorry {
		public var car : MovieClip;
		
		public function __FullLorry() {
			this.weightFrame = 285;
			this.isFull = false;
		}
	}
}
