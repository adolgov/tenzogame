package base.stuff {
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;

	import config.Settings;

	import flash.display.MovieClip;
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 */
	public class Harvester extends MovieClip {
		private static const FRAME_DELTA : int = 10;
		public var grain : MovieClip;
		private var grainTw : ITween;
		private var settings : Settings;

		public function Harvester() {
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			grain = getChildAt(0)['grain'] as MovieClip;
		}

		private function onAdded(event : Event) : void {
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			settings = Settings.getInstance();
			grain.visible = false;
			grain.alpha = 0;
			stop();
		}

		public function showGrain() : void {
			grain.visible = true;
			if (grainTw) grainTw.stop();
			grainTw = BetweenAS3.to(grain, {alpha:1});
			grainTw.play();
		}

		public function hideGrain() : void {
			if (grainTw) grainTw.stop();
			grainTw = BetweenAS3.to(grain, {alpha:0});
			grainTw.onComplete = onGrainHided;
			grainTw.play();
		}

		override public function play() : void {
			if (this.currentFrame == 1 || this.currentFrame == this.totalFrames) return; 
			if (!hasEventListener(Event.ENTER_FRAME))
				this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}

		override public function gotoAndPlay(frame : Object, scene : String = null) : void {
			if (!hasEventListener(Event.ENTER_FRAME))
				this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.gotoAndPlay(frame, scene);
		}

		public function launch() : void {
			if (!hasEventListener(Event.ENTER_FRAME))
				this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.gotoAndPlay(2);
		}

		override public function stop() : void {
			if (hasEventListener(Event.ENTER_FRAME))
				this.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			super.stop();
			hideGrain();
		}

		private function onEnterFrame(event : Event) : void {
			if (settings.quickMode) {
				playFast();
			}
		}

		private function playFast() : void {
			if (this.currentFrame < this.totalFrames - FRAME_DELTA) 
				this.gotoAndPlay(this.currentFrame + FRAME_DELTA);
		}

		private function onGrainHided() : void {
			grain.visible = false;
		}
	}
}
