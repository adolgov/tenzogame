package config {
	/**
	 * @author alexanderdolgov
	 */
	public class DefaultSettings {
		public static const METAL_SETTINGS : GameConfig = new GameConfig({
			/* Значения */
			"normal":17000,
			"addMin":900,
			"addMax":1800,
		
			/* Нормальная погрешность для ЧВ */
			"normalMin":0,
			"normalMax":20,
			"normalStep":20,
		
			/* Погрешность для УПВ */
			"deltaMin":20,
			"deltaMax":60,
			"deltaStep":20,
		
			/* Погрешность для УПВ в жару */
			"deltaHotMin":20,
			"deltaHotMax":60,
			"deltaHotStep":20,
		
			/* Погрешность для УПВ в жару */
			"deltaHotMin":20,
			"deltaHotMax":60,
			"deltaHotStep":20,
		
			/* Стоимость килограмма */
			"multiplier":6.5
		});
		
		public static const CORN_SETTINGS : GameConfig = new GameConfig({
			/* Значения */
			"normal":8600,
			"addMin":400,
			"addMax":500,
		
			/* Нормальная погрешность для ЧВ */
			"normalMin":0,
			"normalMax":20,
			"normalStep":20,
		
			/* Погрешность для УПВ */
			"deltaMin":20,
			"deltaMax":60,
			"deltaStep":20,
		
			/* Погрешность для УПВ в дождь */
			"deltaColdMin":20,
			"deltaColdMax":80,
			"deltaColdStep":20,
		
			/* Погрешность для УПВ в жару */
			"deltaHotMin":20,
			"deltaHotMax":100,
			"deltaHotStep":20,
		
			/* Стоимость килограмма */
			"multiplier":5
		});
	}
}
