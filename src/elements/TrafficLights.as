package elements {
	import config.Settings;
	import flash.utils.setTimeout;
	import events.GameEvent;
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.display.Sprite;

	/**
	 * @author alexanderdolgov
	 */
	public class TrafficLights extends Sprite {
		
		public var trafficlight_in : MovieClip;
		public var trafficlight_out : MovieClip;
		private const ON : int = 1;
		private const OFF : int = 2;
		public function TrafficLights() {
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(event : Event) : void {
			trafficlight_in.gotoAndStop(ON);
			trafficlight_out.gotoAndStop(ON);
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			stage.addEventListener(GameEvent.CAR_INCOMING, onIncoming);
			stage.addEventListener(GameEvent.CAR_RETURN, onCarReturn);
		}

		private function onCarReturn(event : GameEvent) : void {
			trafficlight_out.gotoAndStop(OFF);
			trafficlight_in.gotoAndStop(OFF);
			setTimeout(trafficlight_out.gotoAndStop, Settings.getInstance().quickMode ? 500 : 1500, ON);
			setTimeout(trafficlight_in.gotoAndStop, Settings.getInstance().quickMode ? 250 : 1000, ON);
		}

		private function onIncoming(event : GameEvent) : void {
			trafficlight_out.gotoAndStop(OFF);
			trafficlight_in.gotoAndStop(OFF);
			setTimeout(trafficlight_out.gotoAndStop, Settings.getInstance().quickMode ? 250 : 1000, ON);
			setTimeout(trafficlight_in.gotoAndStop, Settings.getInstance().quickMode ? 500 : 1500, ON);
		}
	}
}
