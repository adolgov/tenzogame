package events {
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 */
	public class SettingsEvent extends Event {
		public static const SETTINGS_CHANGED : String = "settingsChanged";

		public function SettingsEvent(type : String, bubbles : Boolean = true, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
		}

		public override function clone() : Event {
			return new SettingsEvent(type, bubbles, cancelable);
		}

		public override function toString() : String {
			return formatToString("SettingsEvent", "type", "bubbles", "cancelable", "eventPhase");
		}
	}
}
