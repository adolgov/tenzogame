package events {
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 */
	public class WeatherEvent extends Event {
		public static const CHANGE : String = "change";
		public var obj : Object = null;

		public function WeatherEvent(type : String, bubbles : Boolean = true, cancelable : Boolean = false, obj : Object = null) {
			this.obj = obj;
			super(type, bubbles, cancelable);
		}

		public override function clone() : Event {
			return new WeatherEvent(type, bubbles, cancelable);
		}

		public override function toString() : String {
			return formatToString("WeatherEvent", "type", "bubbles", "cancelable", "eventPhase", "obj");
		}
	}
}
