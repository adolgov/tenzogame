package events {
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 */
	public class GameEvent extends Event {
		public static const WAY_STARTED : String = "wayStarted";
		public static const WAY_FINISHED : String = "wayFinished";
		public static const IS_FILLING : String = "isFilling";
		public static const IS_WEIGHTING : String = "isWeighting";
		public static const IS_PREWEIGHTING : String = "isPreweighting";
		public static const SPEED_CHANGED : String = "speedChanged";
		public static const GAME_COMPLETED : String = "gameCompleted";
		public static const PLAY_AGAIN : String = "playAgain";
		public static const PAUSE : String = "pause";
		public static const SELECT_GAME : String = "selectGame";
		public static const CONTINUE : String = "continue";
		public static const CAR_INCOMING : String = "incoming";
		public static const CAR_RETURN : String = "return";
		public var obj : Object = null;

		public function GameEvent(type : String, bubbles : Boolean = true, cancelable : Boolean = false, obj : Object = null) {
			this.obj = obj;
			super(type, bubbles, cancelable);
		}

		public override function clone() : Event {
			return new GameEvent(type, bubbles, cancelable);
		}

		public override function toString() : String {
			return formatToString("GameEvent", "type", "bubbles", "cancelable", "eventPhase", "obj");
		}
	}
}
