package sound {
	import flash.media.Sound;
	import flash.system.ApplicationDomain;

	/**
	 * @author alexanderdolgov
	 */
	public class SoundCollection {
		public var airplane : Sound;
		public var blizzard : Sound;
		public var brake : Sound;
		public var buyitem : Sound;
		public var engine : Sound;
		public var forest : Sound;
		public var harvester : Sound;
		public var harvester2 : Sound;
		public var lightrain : Sound;
		public var musicGame : Sound;
		public var musicMenu : Sound;
		public var squirell : Sound;
		public var summer : Sound;
		public var traffic : Sound;
		public var truck : Sound;
		public var water : Sound;

		public function SoundCollection() {
		}

		public function createCollection() : void {
			var Airplane : Class = ApplicationDomain.currentDomain.getDefinition('Airplane') as Class;
			var Blizzard : Class = ApplicationDomain.currentDomain.getDefinition('Blizzard') as Class;
			var Brake : Class = ApplicationDomain.currentDomain.getDefinition('Brake') as Class;
			var Buyitem : Class = ApplicationDomain.currentDomain.getDefinition('Buyitem') as Class;
			var Engine : Class = ApplicationDomain.currentDomain.getDefinition('Engine') as Class;
			var Forest : Class = ApplicationDomain.currentDomain.getDefinition('Forest') as Class;
			var Harverter : Class = ApplicationDomain.currentDomain.getDefinition('Harvester') as Class;
			var Harvester2 : Class = ApplicationDomain.currentDomain.getDefinition('Harvester2') as Class;
			var Lightrain : Class = ApplicationDomain.currentDomain.getDefinition('Lightrain') as Class;
			var MusicGame : Class = ApplicationDomain.currentDomain.getDefinition('MusicGame') as Class;
			var MusicMenu : Class = ApplicationDomain.currentDomain.getDefinition('MusicMenu') as Class;
			var Squirell : Class = ApplicationDomain.currentDomain.getDefinition('Squirell') as Class;
			var Summer : Class = ApplicationDomain.currentDomain.getDefinition('Summer') as Class;
			var Traffic : Class = ApplicationDomain.currentDomain.getDefinition('Traffic') as Class;
			var Truck : Class = ApplicationDomain.currentDomain.getDefinition('Truck') as Class;
			var Water : Class = ApplicationDomain.currentDomain.getDefinition('Water') as Class;

			airplane = new Airplane();
			blizzard = new Blizzard();
			brake = new Brake();
			buyitem = new Buyitem();
			engine = new Engine();
			forest = new Forest();
			harvester = new Harverter();
			harvester2 = new Harvester2();
			lightrain = new Lightrain();
			musicGame = new MusicGame();
			musicMenu = new MusicMenu();
			squirell = new Squirell();
			summer = new Summer();
			traffic = new Traffic();
			truck = new Truck();
			water = new Water();
		}
	}
}
