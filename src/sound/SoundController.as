package sound {
	import flash.utils.Dictionary;
	import flash.media.SoundChannel;

	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.media.Sound;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;

	/**
	 * @author alexanderdolgov
	 */
	public class SoundController {
		private static var instance : SoundController = new SoundController();
		private const SHIFTER : Number = .4;
		
		public var vol : Number = 1;
		public var sounds : SoundCollection;
		private var sndTw : ITween;
		private var musicChannel : SoundChannel = new SoundChannel();
		private var carsSounds : Dictionary = new Dictionary();

		public function SoundController() {
			if ( instance ) throw new Error("Settings is singleton and it can only be accessed through Settings.getInstance()");
		}

		public static function getInstance() : SoundController {
			return instance;
		}

		/* P L A Y I N G   S O U N D S  */
		public function playMusic(track : Object) : void {
			var snd : Sound;
			if (track is String) snd = sounds[track] as Sound;
			else if (track is Sound) snd = track as Sound;

			if (snd) {
				if (musicChannel) musicChannel.stop();
				musicChannel = snd.play(0, int.MAX_VALUE, new SoundTransform(.4));
			} else throw new Error("There's no such an effect sound!");
		}

		public function playEffect(eff : Object) : void {
			var snd : Sound;
			if (eff is String) snd = sounds[eff] as Sound;
			else if (eff is Sound) snd = eff as Sound;

			if (snd) snd.play(0, 0, new SoundTransform(.5));
			else throw new Error("There's no such an effect sound!");
		}

		public function playCarSound(car : Object) : void {
			if (!carsSounds[car]) {
				carsSounds[car] = new SoundChannel();
				carsSounds[car] = sounds.harvester.play(0, int.MAX_VALUE, new SoundTransform(.5));
			}
		}

		public function stopCarSound(car : Object) : void {
			if (carsSounds[car] && carsSounds[car] is SoundChannel) {
				(carsSounds[car] as SoundChannel).stop();
				carsSounds[car] = null;
			}
		}

		/* V O L U M E */
		/**
		 * Switching global volume to needed level
		 */
		public function switchSoundTo(lvl : Number) : void {
			if (sndTw) sndTw.stop();
			sndTw = BetweenAS3.to(this, {vol:lvl * SHIFTER}, 2);
			sndTw.onUpdate = onVolumeUpdate;
			sndTw.play();
		}

		private function onVolumeUpdate() : void {
			SoundMixer.soundTransform = new SoundTransform(vol);
		}
	}
}
