package sound {
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;

	/**
	 * @author alexanderdolgov
	 */
	public class SoundLoader {
		private static var ldr : Loader;
		private static var onSoundsReady : Function;

		public static function loadSound(soundsReady : Function = null) : void {
			if (soundsReady != null) onSoundsReady = soundsReady;
			var url : URLRequest = new URLRequest('content/sounds.swf');
			var context : LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);
			ldr = new Loader();
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, onSoundsLoaded);
			ldr.load(url, context);
		}

		private static function onSoundsLoaded(e : Event) : void {
			ldr.removeEventListener(Event.COMPLETE, onSoundsLoaded);
			var sndCollection : SoundCollection = new SoundCollection();
			sndCollection.createCollection();
			SoundController.getInstance().sounds = sndCollection;
			if (onSoundsReady != null) onSoundsReady();
			trace('sounds loaded');
		}
	}
}
