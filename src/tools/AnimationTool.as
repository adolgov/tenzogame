package tools {
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	/**
	 * @author alexanderdolgov
	 */
	public class AnimationTool {
		public static function stopAllChilds(mc : DisplayObjectContainer) : void {
			if (mc is MovieClip) (mc as MovieClip).stop();
			for (var i : int = 0; i < mc.numChildren; i++) {
				if (mc.getChildAt(i) is DisplayObjectContainer)
					stopAllChilds(mc.getChildAt(i) as DisplayObjectContainer);
			}
		}

		public static function playAllChilds(mc : DisplayObjectContainer) : void {
			if (mc is MovieClip) (mc as MovieClip).play();
			for (var i : int = 0; i < mc.numChildren; i++) {
				if (mc.getChildAt(i) is DisplayObjectContainer)
					playAllChilds(mc.getChildAt(i) as DisplayObjectContainer);
			}
		}
	}
}
