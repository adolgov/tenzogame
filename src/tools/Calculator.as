package tools {
	import config.GameConfig;

	/**
	 * @author alexanderdolgov
	 */
	public class Calculator {
		public static function calculateWeight(cfg : GameConfig, weather : String = "norm") : WeightResults {
			// trace("Calclulating for: " + cfg, weather);
			var baseWeight : Number = Math.round(cfg.normal + negativer() * randomer(cfg.addMin, cfg.addMax, 20));
			// results
			var results : WeightResults = new WeightResults();
			results.baseWeight = baseWeight;
			results.cleanWeihgt = baseWeight + negativer() * randomer(cfg.normalMin, cfg.normalMax, cfg.normalStep);

			switch(weather) {
				case "cold":
					results.dirtyWeight = baseWeight + randomer(cfg.deltaColdMin, cfg.deltaColdMax, cfg.deltaColdStep);
					break;
				case "heat":
					results.dirtyWeight = baseWeight + randomer(cfg.deltaHotMin, cfg.deltaHotMax, cfg.deltaHotStep);
					break;
				default:
					results.dirtyWeight = baseWeight + randomer(cfg.deltaMin, cfg.deltaMax, cfg.deltaStep);
					break;
			}

			results.difference = results.dirtyWeight - results.cleanWeihgt;
			results.economy = results.difference * cfg.multiplier;
			return results;
		}

		public static function recalculateWeight(cleanWeight : Number, dirtyWeight : Number, cfg : GameConfig) : WeightResults {
			var results : WeightResults = new WeightResults();
			results.cleanWeihgt = cleanWeight;
			results.dirtyWeight = dirtyWeight;
			results.difference = results.dirtyWeight - results.cleanWeihgt;
			results.economy = results.difference * cfg.multiplier;
			return results;
		}

		private static function randomer(min : Number, max : Number, step : Number = 1) : Number {
			var stepsAmount : Number = (max - min) / step;
			trace('stepsAmount: ' + (stepsAmount));
			var delta : Number = step * Math.round(stepsAmount * Math.random());
			trace('delta: ' + (delta));
			return min + delta;
		}

		private static function negativer() : Number {
			var rand : Number = (Math.random() >= 0.5) ? 1 : -1;
			return rand;
		}

		public static function short(weihgt : Number) : String {
			var weightString : String = String(Math.round(weihgt / 100) / 10);
			weightString = replace(weightString, ".", ",");
			return weightString;
		}

		private static function replace(str : String, fnd : String, rpl : String) : String {
			return str.split(fnd).join(rpl);
		}

		public static function cyrNumeric(num : Number, $many : String, $one : String, $two : String) : String {
			num = int(Math.abs(num));
			var str : String;
			if ( num % 100 == 1 || (num % 100 > 20) && ( num % 10 == 1 ) ) str = $one;
			else if ( num % 100 == 2 || (num % 100 > 20) && ( num % 10 == 2 ) ) str = $two;
			else if ( num % 100 == 3 || (num % 100 > 20) && ( num % 10 == 3 ) ) str = $two;
			else if ( num % 100 == 4 || (num % 100 > 20) && ( num % 10 == 4 ) ) str = $two;
			else str = $many;
			return num.toString() + ' ' + str;
		}
	}
}
