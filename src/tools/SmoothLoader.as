package tools {
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.easing.Quint;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;

	/**
	 * @author alexanderdolgov
	 * Класс для замены Loader для загрузки и аккуратного отображения Bitmaps
	 * При смене url отображает его с фейдом, убирая фейдом старые картинку снизу
	 */
	public class SmoothLoader extends Sprite {
		public var content : DisplayObject;
		// private
		private var _src : String;
		private var ldr : Loader;
		private var size : Rectangle;
		// tweens
		private var tweens : Dictionary;

		// =================== C O N S T R U C T O R ======================= //
		/**
		 * Конструктор, опционально ограниченный по высоте и ширине
		 */
		public function SmoothLoader(width : Number = NaN, height : Number = NaN) {
			tweens = new Dictionary();
			if (width && height) {
				size = new Rectangle(0, 0, width, height);
			}
		}

		public function dismiss() : void {
			if (ldr) {
				if (ldr.contentLoaderInfo.hasEventListener(Event.COMPLETE)) {
					ldr.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoadComplete);
					ldr.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onLoadError);
				}
				try {
					ldr.close();
				} catch(e : Error) {
				}
			}
		}

		// ================ P R I V A T E  M E T H O D S =================== //
		/**
		 * Загрузка объекта
		 */
		private function load(url : String) : void {
			ldr = new Loader();
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
			ldr.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onLoadError);
			ldr.load(new URLRequest(url));
		}

		// ======================== H A N D L E R S ======================== //
		/**
		 * Когда происходит ошибка загрузки
		 */
		private function onLoadError(event : IOErrorEvent) : void {
			trace("Loading error for url: " + src);
			ldr.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoadComplete);
			ldr.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onLoadError);
			ldr = null;
		}

		/**
		 * Объект удачно загрузился
		 */
		protected function onLoadComplete(event : Event) : void {
			this.dispatchEvent(new Event(Event.COMPLETE));
			ldr.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoadComplete);
			ldr.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onLoadError);
			content = ldr.content;
			if (content is Bitmap) (content as Bitmap).smoothing = true;
			ldr.unload();
			ldr = null;

			//
			if (size) {
				content.width = size.width;
				content.height = size.height;
			}
			content.alpha = 0;
			this.addChild(content);
			// appearing
			if (tweens[content]) (tweens[content] as ITween).stop();
			tweens[content] = BetweenAS3.to(content, {alpha:1}, 1, Quint.easeOut);
			(tweens[content] as ITween).play();
			onContentAppeared();
		}

		private function onContentAppeared() : void {
			for (var i : int = 0; i < this.numChildren; i++) {
				var tgt : DisplayObject = this.getChildAt(i);
				if (tgt != content) {
					if (tweens[tgt]) (tweens[tgt] as ITween).stop();
					tweens[tgt] = BetweenAS3.to(tgt, {alpha:0}, 1, Quint.easeOut);
					(tweens[tgt] as ITween).onComplete = onTrashHided;
					(tweens[tgt] as ITween).onCompleteParams = [tgt];
					(tweens[tgt] as ITween).play();
				}
			}
		}

		private function onTrashHided(tgt : DisplayObject) : void {
			if (tgt.parent) tgt.parent.removeChild(tgt);
		}

		// ============== G E T T E R S  A N D  S E T T E R S =============== //
		/**
		 * Параметр источника фотки, при установке которого происходит подгрузка фотки и ее отображениe
		 */
		public function get src() : String {
			return _src;
		}

		public function set src(s : String) : void {
			_src = s;
			load(s);
		}
	}
}
