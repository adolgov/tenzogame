package tools {
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 * Класс для замены Loader для загрузки и аккуратного отображения Bitmaps
	 * При смене url отображает его с фейдом, убирая фейдом старые картинку снизу
	 */
	public class SmoothLoaderCentered extends SmoothLoader {
		// =================== C O N S T R U C T O R ======================= //
		/**
		 * Конструктор, опционально ограниченный по высоте и ширине
		 */
		public function SmoothLoaderCentered(width : Number = NaN, height : Number = NaN) {
			super(width, height);
		}

		// ================ P R I V A T E  M E T H O D S =================== //
		// ======================== H A N D L E R S ======================== //
		/**
		 * Объект удачно загрузился
		 */
		override protected function onLoadComplete(event : Event) : void {
			super.onLoadComplete(event);
			super.content.x = -super.content.width / 2;
			super.content.y = -super.content.height / 2;
		}
	}
}
