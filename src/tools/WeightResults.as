package tools {
	/**
	 * @author alexanderdolgov
	 */
	public class WeightResults extends Object {
		public var baseWeight : Number = 0;
		public var cleanWeihgt : Number = 0;
		public var dirtyWeight : Number = 0;
		public var difference : Number = 0;
		public var economy : Number = 0;
		
		public function addResults(resulst : WeightResults) : WeightResults{
			baseWeight += resulst.baseWeight;
			cleanWeihgt += resulst.cleanWeihgt;
			dirtyWeight += resulst.dirtyWeight;
			difference += resulst.difference;
			economy += resulst.economy;
			return this;
		}
		 
	}
}
