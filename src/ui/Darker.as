package ui {
	import sound.SoundController;

	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 */
	public class Darker extends Sprite {
		private var fadeTw : ITween;
		private var snd : SoundController;

		public function Darker(isVisible : Boolean = false) {
			this.mouseEnabled = this.mouseChildren = false;
			alpha = isVisible ? 1 : 0;
			visible = isVisible;
			if (stage) onAddedToStage();
			else addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		public function fadeIn() : void {
			this.visible = true;
			if (fadeTw) fadeTw.stop();
			fadeTw = BetweenAS3.to(this, {alpha:1});
			fadeTw.play();
			snd = SoundController.getInstance();
			if (snd.sounds) snd.playMusic(snd.sounds.musicMenu);
		}

		public function fadeOut() : void {
			if (fadeTw) fadeTw.stop();
			fadeTw = BetweenAS3.to(this, {alpha:0});
			fadeTw.onComplete = onDisappeared;
			fadeTw.play();
			snd = SoundController.getInstance();
			if (snd.sounds) snd.playMusic(snd.sounds.musicGame);
		}

		private function onDisappeared() : void {
			this.visible = false;
		}

		private function onAddedToStage(e : Event = null) : void {
			if (this.hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
	}
}
