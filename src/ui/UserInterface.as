package ui {
	import config.Settings;

	import events.GameEvent;
	import events.SettingsEvent;
	import events.WeatherEvent;

	import sound.SoundController;

	import ui.abstract.AbstractDialog;
	import ui.abstract.AbstractTab;
	import ui.dialogs.EnterDialog;
	import ui.dialogs.FinalDialog;
	import ui.dialogs.HelpDialog;
	import ui.headbuttons.DamageCounter;
	import ui.headbuttons.HelpToggler;
	import ui.headbuttons.SoundToggler;
	import ui.tabs.IterationsTab;
	import ui.tabs.ResultsTab;
	import ui.tabs.TabsSwitcher;
	import ui.tabs.WeatherTab;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author alexanderdolgov
	 */
	public class UserInterface extends Sprite {
		// tabs
		public var tabs : TabsSwitcher;
		public var results : ResultsTab;
		public var weather : WeatherTab;
		public var iterations : IterationsTab;
		// top buttons
		public var counter : DamageCounter;
		public var helpButton : HelpToggler;
		public var soundSwitcher : SoundToggler;
		// fader
		public var darker : Darker;
		// dialogs
		private var enterDialog : EnterDialog;
		private var finalDialog : FinalDialog;
		private var helpDialog : HelpDialog;
		private var currentDialog : AbstractDialog;

		public function UserInterface() {
			results.hide();
			iterations.hide();
			if (stage) onAdded();
			else addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			tabs.resultsButton.addEventListener(MouseEvent.CLICK, onSwitcherClicked);
			tabs.weatherButton.addEventListener(MouseEvent.CLICK, onSwitcherClicked);
			tabs.iterationsButton.addEventListener(MouseEvent.CLICK, onSwitcherClicked);
			// top button listeners
			soundSwitcher.addEventListener(MouseEvent.CLICK, onSoundSwitched);
			helpButton.addEventListener(MouseEvent.CLICK, onHelpClicked);
			counter.addEventListener(MouseEvent.CLICK, onCounterClicked);
			// initial stage listeners
			initStageListeners();
		}

		private function initStageListeners() : void {
			if (!stage.hasEventListener(GameEvent.GAME_COMPLETED)) {
				stage.addEventListener(GameEvent.GAME_COMPLETED, onGameCompleted);
				stage.addEventListener(GameEvent.PAUSE, onGamePaused);
			}
		}

		private function onCounterClicked(event : MouseEvent) : void {
			if (currentDialog == enterDialog) return;
			this.dispatchEvent(new GameEvent(GameEvent.PAUSE, true));
		}

		private function onHelpClicked(event : MouseEvent) : void {
			showHelpDialog();
		}

		private function onSoundSwitched(event : MouseEvent) : void {
			if (soundSwitcher.currentFrame == 1) SoundController.getInstance().switchSoundTo(1);
			else SoundController.getInstance().switchSoundTo(0);
		}

		private function onGameCompleted(event : GameEvent) : void {
			// stage.removeEventListener(GameEvent.GAME_COMPLETED, onGameCompleted);
			showFinalDialog();
		}

		/**
		 * Switcher handled switching
		 */
		private function onSwitcherClicked(e : MouseEvent) : void {
			tabs.disable();
			var tgt : String = e.currentTarget['name'];
			switch(tgt) {
				case "resultsButton":
					switchTo(results);
					break;
				case "weatherButton":
					switchTo(weather);
					break;
				case "iterationsButton":
					switchTo(iterations);
					break;
				default:
					break;
			}
		}

		/**
		 * Tabs switching, hiding all tabs and showing needed one
		 */
		private function switchTo(tab : AbstractTab) : void {
			results.hide();
			weather.hide();
			iterations.hide();
			tab.show(onTabAppeared);
		}

		private function onTabAppeared() : void {
			tabs.enable();
		}

		private function onGamePaused(event : GameEvent) : void {
			trace("game paused");
			showFinalDialog(true);
		}

		//																	  //
		// –------––––----––––– F I N A L  D I A L O G ––––––---–----–––––––––//
		//																	  //
		public function showFinalDialog(continueAvailable : Boolean = false) : void {
			darker.fadeIn();
			if (!finalDialog) finalDialog = new FinalDialog();
			finalDialog.next.visible = continueAvailable;
			addDialogListeners();
			finalDialog.showAt(this);
			currentDialog = finalDialog;
		}

		private function onGameContinue(event : GameEvent) : void {
			removeDialogListeners();
			darker.fadeOut();
			finalDialog.hide();
			currentDialog = null;
		}

		private function onPlayAgain(event : GameEvent) : void {
			removeDialogListeners();
			darker.fadeOut();
			finalDialog.hide();
			currentDialog = null;
		}

		private function onSelectNewGame(event : GameEvent) : void {
			removeDialogListeners();
			finalDialog.hide();
			currentDialog = null;
			showStartDialog();
		}

		//																	  //
		// –------––––----––––– S T A R T  D I A L O G ––––––---–----–––––––––//
		//																	  //
		public function showStartDialog() : void {
			darker.fadeIn();
			if (!enterDialog) enterDialog = new EnterDialog();
			enterDialog.addEventListener(Event.COMPLETE, onDialogSkipped);
			enterDialog.showAt(this);
			this.dispatchEvent(new WeatherEvent(WeatherEvent.CHANGE, true, false, "norm"));
			currentDialog = enterDialog;
		}

		private function onDialogSkipped(event : Event) : void {
			Settings.getInstance().reset();
			this.dispatchEvent(new SettingsEvent(SettingsEvent.SETTINGS_CHANGED, true));
			enterDialog.removeEventListener(Event.COMPLETE, onDialogSkipped);
			enterDialog.hide();
			this.dispatchEvent(new Event("dialogSkipped"));
			darker.fadeOut();
			currentDialog = null;
		}

		//																	  //
		// –------––––----––––– H E L P    D I A L O G ––––––---–----–––––––––//
		//																	  //
		public function showHelpDialog() : void {
			darker.fadeIn();
			if (currentDialog) currentDialog.hide(false);
			if (!helpDialog) helpDialog = new HelpDialog();
			helpDialog.addEventListener(Event.COMPLETE, onHelpDialogSkipped);
			helpDialog.showAt(this);
			this.dispatchEvent(new Event("dialogPaused"));
		}

		private function onHelpDialogSkipped(event : Event) : void {
			helpDialog.removeEventListener(Event.COMPLETE, onDialogSkipped);
			helpDialog.hide(false);
			if (currentDialog) {
				currentDialog.showAt(this);
			} else {
				this.dispatchEvent(new Event("gameResumed"));
				darker.fadeOut();
			}
		}

		/**
		 * Adding and remove listeners for final dialog
		 */
		private function addDialogListeners() : void {
			if (finalDialog.hasEventListener(GameEvent.PLAY_AGAIN)) return;
			finalDialog.addEventListener(GameEvent.PLAY_AGAIN, onPlayAgain);
			finalDialog.addEventListener(GameEvent.SELECT_GAME, onSelectNewGame);
			finalDialog.addEventListener(GameEvent.CONTINUE, onGameContinue);
		}

		private function removeDialogListeners() : void {
			if (!finalDialog.hasEventListener(GameEvent.PLAY_AGAIN)) return;
			finalDialog.removeEventListener(GameEvent.PLAY_AGAIN, onPlayAgain);
			finalDialog.removeEventListener(GameEvent.SELECT_GAME, onSelectNewGame);
			finalDialog.removeEventListener(GameEvent.GAME_COMPLETED, onSelectNewGame);
		}
	}
}
