package ui.abstract {
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.easing.Quint;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author adolgov
	 */
	public class AbstractButton extends MovieClip {
		// possible animation movie
		public var over : MovieClip;
		public var down : MovieClip;
		private var tw : ITween;

		public function AbstractButton() {
			this.buttonMode = this.mouseEnabled = true;
			this.mouseChildren = false;
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			if (over) over.mouseEnabled = over.mouseChildren = false;
			if (down) down.mouseEnabled = down.mouseChildren = false;
		}

		public function show() : void {
			if (tw) tw.stop();
			tw = BetweenAS3.to(this, {alpha:1}, 2, Quint.easeOut);
			tw.play();
		}

		private function onAddedToStage(event : Event) : void {
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			this.addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			this.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			this.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onRemovedFromStage(event : Event) : void {
			this.removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			this.removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			this.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			this.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onMouseDown(event : MouseEvent) : void {
			if (down) playAnimation(down);
		}

		private function onMouseUp(event : MouseEvent) : void {
			if (down) down.addEventListener(Event.ENTER_FRAME, playAnimationBack);
		}

		private function onMouseOver(event : MouseEvent) : void {
			if (over) playAnimation(over);
		}

		private function onMouseOut(event : MouseEvent) : void {
			if (over) over.addEventListener(Event.ENTER_FRAME, playAnimationBack);
		}

		/* ––––––––––––– A D D I C T I V E   F U N C T I O N S  ––––––––––––––– */
		/**
		 * playing animation on mouseover or mouseown
		 */
		private function playAnimation(mc : MovieClip) : void {
			if (mc.hasEventListener(Event.ENTER_FRAME))
				mc.removeEventListener(Event.ENTER_FRAME, playAnimationBack);
			mc.play();
		}
		
		/**
		 * playing movieclip back (enter frame handler)
		 */
		private function playAnimationBack(e : Event) : void {
			var tgt : MovieClip = e.currentTarget as MovieClip;
			if (tgt.currentFrame > 1) {
				tgt.prevFrame();
			} else tgt.removeEventListener(Event.ENTER_FRAME, playAnimationBack);
		}
	}
}
