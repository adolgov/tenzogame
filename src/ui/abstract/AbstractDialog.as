package ui.abstract {
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;
	import flash.display.Sprite;
	/**
	 * @author alexanderdolgov
	 */
	public class AbstractDialog extends Sprite{
		private var tw : ITween;
		public function AbstractDialog(){
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(event : Event) : void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			this.x = 450;
			this.y = -260;
		}
		
		public function showAt(cont : DisplayObjectContainer) : void {
			this.alpha = 0;
			cont.addChild(this);
			if (tw) tw.stop();
			tw = BetweenAS3.to(this,{alpha:1});
			tw.play();
		}
		
		public function hide(dispatching : Boolean = true):void{
			if (tw) tw.stop();
			tw = BetweenAS3.to(this,{alpha:0});
			if (dispatching) tw.onComplete = onDisappear;
			tw.play();
		}

		private function onDisappear() : void {
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
	}
}
