package ui.abstract {
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.easing.Quint;
	import org.libspark.betweenas3.easing.Quintic;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.Sprite;

	/**
	 * @author alexanderdolgov
	 */
	public class AbstractTab extends Sprite {
		public var tw : ITween;
		protected const GREEN : String = "#669a28";
		protected const GREY : String = "#999999";
		protected const RED : String = "#f65d0e";
		
		public function AbstractTab() {
		}


		/**
		 * Hiding
		 */
		public function hide(onHidedPassed : Function = null) : void {
			if (tw) tw.stop();
			tw = BetweenAS3.to(this, {alpha:0}, 1.3, Quint.easeOut);
			if (onHidedPassed != null) tw.onCompleteParams = [onHidedPassed];
			tw.onComplete = onHided;
			tw.play();
		}

		private function onHided(passed : Function = null) : void {
			if (passed != null) passed();
			this.disable();
		}

		/**
		 * Showing
		 */
		public function show(onShown : Function = null) : void {
			enable();
			if (tw) tw.stop();
			tw = BetweenAS3.to(this, {alpha:1}, 1, Quintic.easeOut);
			if (onShown != null) tw.onComplete = onShown;
			tw.play();
		}

		/* ––––––––––– ENABLING AND DISABLING –––––––––––––– */
		/**
		 * disabling tab
		 */
		private function disable() : void {
			this.mouseEnabled = this.mouseChildren = false;
			this.visible = false;
		}

		/**
		 * enabling tab
		 */
		private function enable() : void {
			this.mouseEnabled = this.mouseChildren = true;
			this.visible = true;
		}
	}
}
