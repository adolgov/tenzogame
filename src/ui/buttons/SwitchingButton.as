package ui.buttons {
	import ui.abstract.AbstractButton;

	import flash.display.Sprite;

	/**
	 * @author alexanderdolgov
	 */
	public class SwitchingButton extends AbstractButton {
		public var selected : Sprite; 
		private var _switched : Boolean = false;

		public function SwitchingButton() {
			super();
			selected.visible = switched;
		}

		public function get switched() : Boolean {
			return _switched;
		}

		public function set switched(s : Boolean) : void {
			_switched = switched;
			over.visible = !s;
			selected.visible = s;
		}
		
		
	}
}
