package ui.dialogs {
	import config.DefaultSettings;
	import config.GameConfig;
	import config.Settings;

	import events.SettingsEvent;

	import ui.abstract.AbstractButton;
	import ui.abstract.AbstractDialog;
	import ui.buttons.SwitchingButton;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author alexanderdolgov
	 */
	public class EnterDialog extends AbstractDialog {
		public var b01 : SwitchingButton;
		public var b02 : SwitchingButton;
		public var plus : AbstractButton;
		public var minus : AbstractButton;
		public var next : AbstractButton;
		public var waysTF : TextField;
		//
		private var settings : Settings;

		//
		public function EnterDialog() {
			settings = Settings.getInstance();
			b01.addEventListener(MouseEvent.CLICK, onButtonClicked);
			b02.addEventListener(MouseEvent.CLICK, onButtonClicked);
			plus.addEventListener(MouseEvent.CLICK, onButtonClicked);
			minus.addEventListener(MouseEvent.CLICK, onButtonClicked);
			next.addEventListener(MouseEvent.CLICK, onButtonClicked);
			b01.switched = true;
		}

		private function onButtonClicked(e : MouseEvent) : void {
			var tgt : AbstractButton = e.target as AbstractButton;
			e.stopImmediatePropagation();
			switch(tgt) {
				case b01:
					settings.currentConfig = (settings.data) ? settings.data["corn"] as GameConfig : DefaultSettings.CORN_SETTINGS;
					settings.currentGame = "metal";
					this.dispatchEvent(new SettingsEvent(SettingsEvent.SETTINGS_CHANGED, true));
					b02.switched = false;
					b01.switched = true;
					break;
				case b02:
					settings.currentConfig = (settings.data) ? settings.data["metal"] as GameConfig : DefaultSettings.METAL_SETTINGS;
					settings.currentGame = "corn";
					this.dispatchEvent(new SettingsEvent(SettingsEvent.SETTINGS_CHANGED, true));
					b01.switched = false;
					b02.switched = true;
					break;
				case plus:
					settings.waysAmount += 10;
					waysTF.text = String(settings.waysAmount);
					break;
				case minus:
					settings.waysAmount -= 10;
					waysTF.text = String(settings.waysAmount);
					break;
				case next:
					this.dispatchEvent(new Event(Event.COMPLETE));
					break;
				default:
					break;
			}
		}
	}
}
