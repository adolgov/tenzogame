package ui.dialogs {
	import tools.Calculator;
	import events.GameEvent;

	import flash.events.MouseEvent;

	import ui.abstract.AbstractButton;

	import config.Settings;

	import ui.abstract.AbstractDialog;

	import flash.display.DisplayObjectContainer;
	import flash.text.TextField;

	/**
	 * @author alexanderdolgov
	 */
	public class FinalDialog extends AbstractDialog {
		public var textTF : TextField;
		public var monthTF : TextField;
		public var quarterTF : TextField;
		public var next : AbstractButton;
		public var select : AbstractButton;
		public var again : AbstractButton;
		private var settings : Settings;

		public function FinalDialog() {
			settings = Settings.getInstance();
			next.addEventListener(MouseEvent.CLICK, onButtonClicked);
			select.addEventListener(MouseEvent.CLICK, onButtonClicked);
			again.addEventListener(MouseEvent.CLICK, onButtonClicked);
		}

		private function onButtonClicked(event : MouseEvent) : void {
			switch(event.currentTarget) {
				case next:
					this.dispatchEvent(new GameEvent(GameEvent.CONTINUE, true));
					break;
				case select:
					this.dispatchEvent(new GameEvent(GameEvent.PAUSE, true));
					this.dispatchEvent(new GameEvent(GameEvent.SELECT_GAME));
					break;
				case again:
					this.dispatchEvent(new GameEvent(GameEvent.PLAY_AGAIN, true));
					break;
			}
		}

		override public function showAt(cont : DisplayObjectContainer) : void {
			if (settings.totalResults.economy) {
				trace('economy: ' + (settings.totalResults.economy));
				var daily : int = settings.totalResults.economy + ((settings.totalResults.economy / settings.currentRace) * (settings.waysAmount - settings.currentRace));
				trace('daily: ' + (daily));
				var monthly : int = daily * 20;
				trace('monthly: ' + (monthly));
				var selfBuyFor : int = int(settings.data['price']) / monthly;
					
				textTF.text = "С учетом заданного Вами количества рейсов (" + settings.waysAmount + " в день), честные весы окупят себя за " + Calculator.cyrNumeric(selfBuyFor, "месяцев", "месяц", "месяца") + " и начнут приносить прибыль в размере:";
				monthTF.htmlText = "<font size=\"35\">" + monthly + "</font> <font size=\"22\">Руб/месяц</font>";
				quarterTF.htmlText = "<font size=\"35\">" + monthly * 3 + "</font> <font size=\"22\">Руб/квартал</font>";
				monthTF.visible = true;
				quarterTF.visible = true;
			} else {
				var noResults : String = settings.data["noResults"];
				textTF.htmlText = noResults;
				monthTF.visible = false;
				quarterTF.visible = false;
			}
			super.showAt(cont);
		}
	}
}
