package ui.dialogs {
	import flash.utils.setTimeout;
	import config.Settings;

	import tools.SmoothLoader;

	import ui.Scrollbar;
	import ui.abstract.AbstractButton;
	import ui.abstract.AbstractDialog;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author alexanderdolgov
	 */
	public class HelpDialog extends AbstractDialog {
		public var next : AbstractButton;
		public var sb : Scrollbar;
		public var content : Sprite;
		// helpLoader
		private var helpBitmap : SmoothLoader;

		public function HelpDialog() {
			this.addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(event : Event) : void {
			this.removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			if (!helpBitmap) helpBitmap = new SmoothLoader();
			helpBitmap.addEventListener(Event.COMPLETE, onHelpLoaded);
			helpBitmap.src = Settings.getInstance().data['help'];
			content.visible = false;
		}

		private function onHelpLoaded(event : Event) : void {
			helpBitmap.removeEventListener(Event.COMPLETE, onHelpLoaded);
			content.addChild(helpBitmap);
			setTimeout(setupInteractions, 500);
		}

		private function setupInteractions() : void {
			next.addEventListener(MouseEvent.CLICK, onContinueButtonClicked);
			sb.init(content, "easeOutBack", 1, true, 5);
			content.visible = true;
		}

		private function onContinueButtonClicked(e : MouseEvent) : void {
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
	}
}
