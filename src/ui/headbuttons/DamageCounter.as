package ui.headbuttons {
	import tools.Calculator;
	import config.Settings;

	import events.SettingsEvent;

	import flash.events.Event;
	import flash.text.TextField;
	import flash.display.Sprite;

	/**
	 * @author alexanderdolgov
	 */
	public class DamageCounter extends Sprite {
		public var damageTF : TextField;
		private var _economy : int = 0;

		public function DamageCounter() {
			if (stage) onAdded();
			else addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			//
			this.buttonMode = true;
			stage.addEventListener(SettingsEvent.SETTINGS_CHANGED, onSettingsChanged);
		}

		private function onSettingsChanged(event : SettingsEvent) : void {
			if (!Settings.getInstance().totalResults) return;
			economy = Settings.getInstance().totalResults.economy;
		}

		public function set economy(e : int) : void {
			var races : String = Calculator.cyrNumeric(Settings.getInstance().currentRace, "рейсов", "рейс", "рейса");
			damageTF.htmlText = '<font size="16">' + e + '</font> <font size="12">Руб/' + races + '</font>';
			_economy = e;
		}
		
	}
}
