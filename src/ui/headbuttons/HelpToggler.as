package ui.headbuttons {
	import flash.display.MovieClip;

	/**
	 * @author alexanderdolgov
	 */
	public class HelpToggler extends MovieClip {
		public function HelpToggler() {
			this.buttonMode = true;
		}
	}
}
