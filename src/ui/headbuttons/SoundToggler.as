package ui.headbuttons {
	import flash.events.Event;
	import sound.SoundController;
	import ui.abstract.AbstractButton;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	/**
	 * @author alexanderdolgov
	 */
	public class SoundToggler extends AbstractButton {
		private const ON : int = 1;
		private const OFF : int = 0;
		
		public function SoundToggler() {
			this.buttonMode = true;
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(event : Event) : void {
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			this.addEventListener(MouseEvent.CLICK, onClick);
			SoundController.getInstance().switchSoundTo(1);
		}

		private function onClick(event : MouseEvent) : void {
			var timeline : MovieClip = MovieClip(this);
			if (timeline.currentFrame == ON){
				timeline.gotoAndStop(2-OFF);
				SoundController.getInstance().switchSoundTo(OFF);
			} else {
				timeline.gotoAndStop(ON);
				SoundController.getInstance().switchSoundTo(ON);
			}
		}
	}
}
