package ui.tabs {
	import flash.events.MouseEvent;

	import ui.abstract.AbstractButton;

	import config.Settings;

	import events.SettingsEvent;

	import ui.abstract.AbstractTab;

	import flash.events.Event;
	import flash.text.TextField;

	/**
	 * @author alexanderdolgov
	 */
	public class IterationsTab extends AbstractTab {
		public var amountTF : TextField;
		public var waysTF : TextField;
		public var minus : AbstractButton;
		public var plus : AbstractButton;
		private var settings : Settings;
		private var _amount : int = 30;
		private var _waysDone : int = 0;

		public function IterationsTab() {
			if (stage) onAddedToStage();
			else addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			settings = Settings.getInstance();
			settings.addEventListener(SettingsEvent.SETTINGS_CHANGED, onSettingsChanged);
			minus.addEventListener(MouseEvent.CLICK, onWaysAmountButtonClicked);
			plus.addEventListener(MouseEvent.CLICK, onWaysAmountButtonClicked);
		}

		private function onWaysAmountButtonClicked(event : MouseEvent) : void {
			if (event.currentTarget == minus) settings.waysAmount -= 10;
			if (event.currentTarget == plus) settings.waysAmount += 10;
		}

		private function onSettingsChanged(event : SettingsEvent) : void {
			waysDone = settings.currentRace;
			amount = settings.waysAmount;
		}

		public function set waysDone(num : int) : void {
			_waysDone = num;
			var numEnd : int = int(String(num).substr(-1));
			waysTF.text = "Выполнено " ;
			waysTF.appendText((num >= 5 && num <= 20) ? num + " рейсов" : (numEnd == 1) ? num + " рейс" : (numEnd > 1 && numEnd < 5) ? num + " рейса" : num + " рейсов");
			waysTF.appendText(" из " + _amount);
		}

		public function set amount(a : int) : void {
			_amount = a;
			amountTF.text = String(a);
			waysDone = _waysDone;
		}
	}
}
