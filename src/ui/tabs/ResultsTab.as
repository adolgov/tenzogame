package ui.tabs {
	import config.Settings;

	import events.GameEvent;
	import events.SettingsEvent;

	import tools.Calculator;

	import ui.abstract.AbstractButton;
	import ui.abstract.AbstractTab;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author alexanderdolgov
	 */
	public class ResultsTab extends AbstractTab {
		// ruling buttons
		public var buttonStop : AbstractButton;
		public var buttonRewind : AbstractButton;
		public var buttonPlay : Sprite;
		// current results
		public var realTF : TextField;
		public var cheapTF : TextField;
		// results per period
		public var monthResults : TextField;
		public var quarterResults : TextField;
		private var settings : Settings;

		public function ResultsTab() {
			if (stage) onAdded();
			else addEventListener(Event.ADDED_TO_STAGE, onAdded);
			settings = Settings.getInstance();
			buttonPlay.visible = buttonPlay.mouseEnabled = buttonPlay.mouseChildren = false;
		}

		private function onAdded(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			stage.addEventListener(SettingsEvent.SETTINGS_CHANGED, onSettingsChanged);
			buttonRewind.addEventListener(MouseEvent.CLICK, onRewindClicked);
			buttonStop.addEventListener(MouseEvent.CLICK, onStopClicked);
		}

		private function onStopClicked(event : MouseEvent) : void {
			this.dispatchEvent(new GameEvent(GameEvent.PAUSE, true));
		}

		private function onRewindClicked(event : MouseEvent) : void {
			this.dispatchEvent(new GameEvent(GameEvent.SPEED_CHANGED, true));
			settings.quickMode = !settings.quickMode;
			buttonPlay.visible = settings.quickMode;
		}

		private function onSettingsChanged(event : SettingsEvent) : void {
			if (!settings.totalResults) return;
			// monthly
			var resultsPerMonth : String = "<font color=\"" + GREEN + "\">" + Calculator.short(settings.totalResults.cleanWeihgt) + " т</font><br />" + "<font color=\"" + GREY + "\">" + Calculator.short(settings.totalResults.dirtyWeight) + " т</font><br />" + "<font color=\"" + RED + "\">" + Calculator.short(Math.abs(settings.totalResults.difference)) + " т</font>";
			monthResults.htmlText = resultsPerMonth;

			// per 3 months
			var resultsPerQuarter : String = "<font color=\"" + GREEN + "\">" + Calculator.short(settings.totalResults.cleanWeihgt * 3) + " т</font><br />" + "<font color=\"" + GREY + "\">" + Calculator.short(settings.totalResults.dirtyWeight * 3) + " т</font><br />" + "<font color=\"" + RED + "\">" + Calculator.short(Math.abs(settings.totalResults.difference * 3)) + " т</font>";
			quarterResults.htmlText = resultsPerQuarter;

			cheapTF.text = Calculator.short(settings.totalResults.dirtyWeight) + " т";
			realTF.text = Calculator.short(settings.totalResults.cleanWeihgt) + " т";
		}
	}
}
