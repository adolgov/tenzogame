package ui.tabs {
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.Sprite;

	/**
	 * @author alexanderdolgov
	 */
	public class TabsSwitcher extends Sprite {
		public var resultsButton : Sprite;
		public var weatherButton : Sprite;
		public var iterationsButton : Sprite;
		
		private var appearTween : ITween;
		
		public function TabsSwitcher() {
		}

		public function enable() : void {
			this.mouseEnabled = this.mouseChildren = true;
			if (appearTween) appearTween.stop();
			appearTween = BetweenAS3.to(this, {alpha : 1});
			appearTween.play();
		}

		public function disable() : void {
			this.mouseEnabled = this.mouseChildren = false;
			if (appearTween) appearTween.stop();
			appearTween = BetweenAS3.to(this, {alpha : 0}, .01);
			appearTween.play();
		}

	}
}
