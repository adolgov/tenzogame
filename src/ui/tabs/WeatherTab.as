package ui.tabs {
	import tools.Calculator;
	import config.Settings;

	import events.SettingsEvent;
	import events.WeatherEvent;

	import ui.abstract.AbstractTab;

	import weather.TempSwitcher;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author alexanderdolgov
	 */
	public class WeatherTab extends AbstractTab {
		public var switcher : TempSwitcher;
		public var realTF : TextField;
		public var cheapTF : TextField;
		private var settings : Settings;

		public function WeatherTab() {
			switcher.addEventListener(MouseEvent.CLICK, onSwitcherClicked);
			if (stage) onAdded();
			else addEventListener(Event.ADDED_TO_STAGE, onAdded);
			settings = Settings.getInstance();
		}

		private function onAdded(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			stage.addEventListener(SettingsEvent.SETTINGS_CHANGED, onSettingsChanged);
		}

		private function onSettingsChanged(event : SettingsEvent) : void {
			if (!settings.totalResults) return;
			realTF.text = Calculator.short(settings.totalResults.cleanWeihgt) + " т";
			cheapTF.text =  Calculator.short(settings.totalResults.dirtyWeight) + " т";
		}

		private function onSwitcherClicked(event : MouseEvent) : void {
			var currentWeather : String = String(event.target['name']);
			if (currentWeather == 'norm') {
				switcher.setNorm();
			} else if (currentWeather == 'heat') {
				switcher.setHeat();
			} else if (currentWeather == 'cold') {
				switcher.setCold();
			}
			this.dispatchEvent(new WeatherEvent(WeatherEvent.CHANGE, true, false, currentWeather));
		}
	}
}
