package weather {
	import flash.display.Sprite;

	/**
	 * @author alexanderdolgov
	 */
	public class Drop extends Sprite {
		public var fallSpeed : int;
		public var windSpeed : int;
		public var dir : String;
		public var hArea : int;
		public var vArea : int;
		
		public function Drop() {
		}
	}
}
