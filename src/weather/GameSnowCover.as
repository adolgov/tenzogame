package weather {
	import events.WeatherEvent;

	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 */
	public class GameSnowCover extends Sprite {
		public var snow1 : Sprite;
		public var snow2 : Sprite;
		private var appearTw : ITween;

		public function GameSnowCover() {
			this.mouseEnabled = this.mouseChildren = this.visible = false;
			if (stage) onAddedToStage();
			else addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(event : Event = null) : void {
			if (this.hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			stage.addEventListener(WeatherEvent.CHANGE, onWeatherChanged);
		}

		private function onWeatherChanged(event : WeatherEvent) : void {
			var w : String = String(event.obj);
			if (w == "cold") {
				this.visible = true;
				if (appearTw) appearTw.stop();
				appearTw = BetweenAS3.serial(BetweenAS3.to(this, {alpha:1}, .5), BetweenAS3.to(snow1, {alpha:1}, 3), BetweenAS3.to(snow2, {alpha:1}, 3));
				appearTw.play();
			} else {
				if (appearTw) appearTw.stop();
				appearTw = BetweenAS3.to(this, {alpha:0});
				appearTw.onComplete = onHided;
				appearTw.play();
			}
		}

		private function onHided() : void {
			this.visible = false;
			this.alpha = 1;
			snow1.alpha = 0;
			snow2.alpha = 0;
		}
	}
}
