package weather {
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DisplacementMapFilter;
	import flash.filters.DisplacementMapFilterMode;
	import flash.geom.Matrix;
	import flash.geom.Point;

	/**
	 * @author alexanderdolgov
	 */
	public class HeatEffect extends Sprite {
		private var targetWidth : Number;
		private var targetHeight : Number;
		private var targetData : BitmapData;
		private var targetBitmap : Bitmap;
		private var target : Sprite;
		private var dispFilter : DisplacementMapFilter;
		private var perlinData : BitmapData;
		private var offset : Array;
		private var seed : Number;
		private var scaleMatrix : Matrix;
		private var bigPerlinData : BitmapData;
		private var appearTw : ITween;
		private var distorted : Boolean = false;

		/*
		 * The warped, psychedelic image is our 'target image'. For instructional purposes we also display the undelying Perlin noise image and the image containing concentric circles before distortion. 
		 */
		public function HeatEffect() {
			if (stage) onAddedToStage();
			else addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			this.alpha = 0;
		}

		private function onAddedToStage(e : Event = null) : void {
			if (this.hasEventListener(Event.ADDED_TO_STAGE)) {
				this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
			this.addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			setTarget();
		}

		private function setTarget() : void {
			var prnt : DisplayObjectContainer = parent.parent as DisplayObjectContainer;
			var ind : int = prnt.getChildIndex(parent);
			target = prnt.getChildAt(ind - 1) as Sprite;
			setPerlinDistortion();
		}

		private function setPerlinDistortion() : void {
			// we got right target
			targetWidth = target.width;
			targetHeight = target.height;
			targetData = new BitmapData(targetWidth, targetHeight, false, 0xFFFFFFFF);
			targetBitmap = new Bitmap(targetData, PixelSnapping.AUTO, true);
			targetBitmap.x = 1;
			targetBitmap.y = 1;
			this.addChild(targetBitmap);

			perlinData = new BitmapData(150, 100, false, 0xFFFFFF);
			scaleMatrix = new Matrix(6, 0, 0, 6, 0, 0);
			bigPerlinData = new BitmapData(900, 600, false, 0xFFFFFF);
			bigPerlinData.draw(perlinData, scaleMatrix, null, null, null, true);
			seed = Math.floor(Math.random() * 10);

			var point1 : Point = new Point(0, 0);
			var point2 : Point = new Point(0, 0);
			offset = [point1, point2];
			perlinData.perlinNoise(50, 5, 3, seed, false, false, 2, true, null);

			dispFilter = new DisplacementMapFilter();
			dispFilter.mapBitmap = bigPerlinData;
			dispFilter.componentX = 1;
			dispFilter.componentY = 1;
			dispFilter.scaleX = 2;
			dispFilter.scaleY = 2;
			dispFilter.mode = DisplacementMapFilterMode.COLOR;
			targetBitmap.filters = [dispFilter];
			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);

			appearTw = BetweenAS3.to(this, {alpha:1}, 2);
			appearTw.play();
		}

		private function onEnterFrame(event : Event) : void {
			targetData.draw(target);
			if (distorted) makeStep();
			else distorted = true;
		}

		private function makeStep() : void {
			offset[0]['x'] += 10;
			offset[1]['y'] += 2;
			perlinData.perlinNoise(50, 2, 3, seed, false, true, 2, true, offset);
			bigPerlinData.draw(perlinData, scaleMatrix, null, null, null, true);
			distorted = false;
		}

		private function onRemovedFromStage(event : Event) : void {
			if (appearTw) appearTw.stop();
			this.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			this.removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
	}
}
