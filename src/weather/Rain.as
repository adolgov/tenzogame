package weather {
	import flash.display.MovieClip;
	import flash.events.Event;

	public class Rain extends MovieClip {
		private var offset : int = 50;
		private var dropsNumber : int;
		private var dropsVector : Vector.<Drop> = new Vector.<Drop>();

		public function init(drops : int, fallSpeed : int, windSpeed : int, hArea : int, vArea : int, dir : String) : void {
			dropsNumber = drops;

			if (dir == "right") {
				offset *= -1;
			}

			for (var i : int = 0; i < drops; i++) {
				var drop : Drop = new Drop();

				drop.fallSpeed = fallSpeed;
				drop.windSpeed = windSpeed;
				drop.dir = dir;
				drop.hArea = hArea;
				drop.vArea = vArea;

				drop.x = Math.random() * (hArea + offset);
				drop.y = Math.random() * vArea;

				//

				drop.scaleX = Math.round(((Math.random() * 1) + 0.3) * 10) / 10;
				drop.scaleY = drop.scaleX;

				//

				dropsVector.push(drop);

				addChild(drop);
			}

			inTheDirection();
		}

		private function inTheDirection() : void {
			for (var i : int = 0; i < dropsNumber; i++) {
				switch (dropsVector[i].dir) {
					case "left" :
						dropsVector[i].addEventListener(Event.ENTER_FRAME, moveLeft);
						break;
					case "right" :
						dropsVector[i].scaleX *= -1;
						dropsVector[i].addEventListener(Event.ENTER_FRAME, moveRight);
						break;
					default :
						trace("There is some error dude...");
				}
			}
		}

		private function moveLeft(e : Event) : void {
			var d : Drop = e.target as Drop;		
			d.x -= d.windSpeed;
			d.y += Math.random() * d.fallSpeed;

			if (d.y > d.vArea + d.height) {
				d.x = Math.random() * (d.hArea + (offset * 2));
				d.y = - d.height;
			}
		}

		private function moveRight(e : Event) : void {
			var d : Drop = e.target as Drop;		
			d.x += d.windSpeed;
			d.y += Math.random() * d.fallSpeed;

			if (d.y > d.vArea + d.height) {
				d.x = Math.random() * (d.hArea - offset * 2) + offset * 2;
				// Check
				d.y = - d.height;
			}
		}
	}
}