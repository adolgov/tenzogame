package weather {
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.Sprite;
	import flash.events.Event;

	public class RainEffect extends Sprite {
		private var _stageWidht : Number , _stageHeight : Number;
		private var appearTw : ITween;

		public function RainEffect() {
			if (stage) onAddedToStage();
			else addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(e : Event = null) : void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			//
			_stageWidht = stage.stageWidth;
			_stageHeight = stage.stageHeight;
			//
			addRain();
		}

		/**
		 * adding snow
		 */
		private function addRain() : void {
			var rain : Rain = new Rain();
			rain.init(300, 50, 5, _stageWidht, _stageHeight, "left");
			addChild(rain);
			if (appearTw) appearTw.stop();
			appearTw = BetweenAS3.to(this, {alpha:1}, 2);
			appearTw.play();
		}

		/**
		 * 
		 */
		private function onRemovedFromStage(event : Event) : void {
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			if (appearTw) appearTw.stop();
		}
	}
}
