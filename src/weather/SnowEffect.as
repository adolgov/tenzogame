package weather {
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.Sprite;
	import flash.events.Event;

	public class SnowEffect extends Sprite {
		private const SNOW_PARTICLE_NUMBER : uint = 1500;
		private var _stageWidht : Number , _stageHeight : Number;
		private var appearTw : ITween;

		public function SnowEffect() {
			if (stage) onAddedToStage();
			else addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(e : Event = null) : void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			//
			_stageWidht = stage.stageWidth;
			_stageHeight = stage.stageHeight;
			//
			addSnow();
		}

		/**
		 * adding snow
		 */
		private function addSnow() : void {
			var i : uint , snowParticle : SnowParticle;

			for ( i = 1; i <= SNOW_PARTICLE_NUMBER; i++ ) {
				addChild(snowParticle = new SnowParticle(_stageWidht, _stageHeight));
				snowParticle.name = i.toString();
			}

			appearTw = BetweenAS3.to(this, {alpha:1}, 2);
			appearTw.play();
		}

		/**
		 * 
		 */
		private function onRemovedFromStage(event : Event) : void {
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			while (this.numChildren > 0) {
				(this.getChildAt(this.numChildren - 1) as SnowParticle).destroy();
			}
			if (appearTw) appearTw.stop();
		}
	}
}
import flash.display.Graphics;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.filters.BlurFilter;

class SnowParticle extends Sprite {
	private const PARTICLE_MAX_SIZE : uint = 20;
	private const MAX_SPPED : uint = 11;
	private const CHANCES_OF_BIG : Number = .005;
	private var _particle : Shape;
	private var _size : Number;
	private var _stageWidth : uint;
	private var _stageHeight : uint;
	private var _sx : Number;
	private var _sy : Number;
	private var _isBig : Boolean = false;
	private var _basePosition : Number;

	public function SnowParticle(stageWidth : uint, stageHeight : uint) : void {
		_stageWidth = stageWidth;
		_stageHeight = stageHeight;
		init();
	}

	private function init() : void {
		checkBig();
		addSnow();
		setPosition();
		setSpeed();
		setFilter();
		moveSnow();
	}

	private function checkBig() : void {
		if ( Math.random() < CHANCES_OF_BIG)
			_isBig = true;
	}

	private function addSnow() : void {
		var g : Graphics , i : uint , pi : Number , angle : Number;

		addChild(_particle = new Shape());

		if ( !_isBig ) {
			_size = Math.random() * PARTICLE_MAX_SIZE / 10;
			_particle.alpha = _size / ( PARTICLE_MAX_SIZE / 10 ) * Math.random() + 0.3;
		} else {
			_size = Math.random() * PARTICLE_MAX_SIZE + PARTICLE_MAX_SIZE / 4;
			_particle.alpha = Math.random() * 0.7 + 0.2;
		}

		g = _particle.graphics;
		g.beginFill(0xffffff);
		g.moveTo(_size, 0);

		pi = Math.PI / 180;
		for ( i = 0; i < 6;i++) {
			angle = 60 * i;
			g.lineTo(_size * Math.cos(pi * angle), _size * Math.sin(pi * angle));
		}
		g.lineTo(_size, 0);
	}

	private function setPosition() : void {
		_particle.x = Math.random() * _stageWidth;
		_particle.y = Math.random() * _stageHeight * -1;

		_basePosition = _size >> 1;
	}

	private function setSpeed() : void {
		var speedY : Number;

		if ( !_isBig )
			speedY = Math.random() * MAX_SPPED + + ( MAX_SPPED >> 1 );
		else
			speedY = Math.random() * MAX_SPPED * 6 + ( MAX_SPPED >> 1 );
		_sy = speedY;
		_sx = ( Math.random() * 1 < 0.6 ) ? Math.random() * ( MAX_SPPED >> 1 ) : Math.random() * ( MAX_SPPED >> 1 ) * -1;
	}

	private function setFilter() : void {
		if ( _isBig )
			_particle.filters = [new BlurFilter(32, 32)];
		else {
			_particle.filters = [];
			_particle.alpha *= .6;
		}
		// else
		// _particle.filters = [new BlurFilter(_size << 1, _size << 1)];
	}

	private function moveSnow() : void {
		_particle.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
	}

	private function enterFrameHandler($evt : Event) : void {
		_particle.scaleX = _particle.scaleY = Math.random() * 1 + 0.8;
		if ( _particle.scaleX > 1 ) _particle.scaleX = _particle.scaleY = 1;

		_particle.x += _sx;
		_particle.y += _sy;

		if ( _particle.y - _basePosition > _stageHeight || _particle.x + _basePosition < 0 || _particle.x - _basePosition > _stageWidth ) {
			setPosition();
			setSpeed();
			// setFilter();
		}
	}

	public function get isBig() : Boolean {
		return _isBig;
	}

	public function destroy() : void {
		_particle.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
		this.parent.removeChild(this);
	}
}