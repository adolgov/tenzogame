package weather {
	import config.Settings;
	import events.SettingsEvent;
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.display.Sprite;

	/**
	 * @author alexanderdolgov
	 */
	public class TempSwitcher extends Sprite {
		public var coldState : Sprite;
		public var heatState : MovieClip;
		public var normState : MovieClip;

		public function TempSwitcher() {
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(event : Event) : void {
			mouseEnabled = false;
			setNorm();
			coldState.mouseEnabled = heatState.mouseEnabled = normState.mouseEnabled = false;
			coldState.mouseChildren = heatState.mouseChildren = normState.mouseChildren = true;
			stage.addEventListener(SettingsEvent.SETTINGS_CHANGED, onGameChanged);
			
		}

		private function onGameChanged(event : SettingsEvent) : void {
			var gameNum : int = Settings.getInstance().currentGame == "metal" ? 1 : 2;
			normState.gotoAndStop(gameNum);
			heatState.gotoAndStop(gameNum);
		}

		public function setCold() : void {
			heatState.visible = false;
			normState.visible = false;
			coldState.visible = true;
		}

		public function setNorm() : void {
			coldState.visible = false;
			heatState.visible = false;
			normState.visible = true;
		}

		public function setHeat() : void {
			coldState.visible = false;
			normState.visible = false;
			heatState.visible = true;
		}
	}
}
