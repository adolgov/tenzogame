package weather {
	import config.Settings;

	import events.WeatherEvent;

	import sound.SoundController;

	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.tweens.ITween;

	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 * Display object creating to reveal weather changes
	 */
	public class WeatherController extends Sprite {
		public var cold : Sprite;
		public var heat : Sprite;
		public var msk : Sprite;
		private var snow : SnowEffect;
		private var rain : RainEffect;
		private var air : HeatEffect;
		private var heatTw : ITween;
		private var coldTw : ITween;
		private var snd : SoundController;

		public function WeatherController() {
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			this.mouseEnabled = this.mouseChildren = false;
			cold.alpha = 0;
			heat.alpha = 0;
		}

		private function onAddedToStage(event : Event) : void {
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			stage.addEventListener(WeatherEvent.CHANGE, onWeatherChanged);
			// sounds
			snd = SoundController.getInstance();
		}

		private function onWeatherChanged(event : WeatherEvent) : void {
			var requiredWeather : String = String(event.obj);
			switch(requiredWeather) {
				case "cold":
					makeCold();
					break;
				case "norm":
					makeNorm();
					break;
				case "heat":
					makeHeat();
					break;
				default:
					break;
			}
			// global changing of weather
			Settings.getInstance().currentWeather = requiredWeather;
		}

		public function makeHeat() : void {
			if (coldTw) coldTw.stop();
			coldTw = BetweenAS3.to(cold, {alpha:0}, 2);
			coldTw.play();
			if (heatTw) heatTw.stop();
			heatTw = BetweenAS3.to(heat, {alpha:1}, 3);
			heatTw.play();
			removeSnow();
			removeRain();
			addHeat();
			
			// playing sounds
			if (snd && snd.sounds) snd.playEffect(snd.sounds.summer);
		}

		public function makeCold() : void {
			if (heatTw) heatTw.stop();
			heatTw = BetweenAS3.to(heat, {alpha:0}, 2);
			heatTw.play();
			if (coldTw) coldTw.stop();
			coldTw = BetweenAS3.to(cold, {alpha:1}, 3);
			coldTw.play();
			//
			if (Settings.getInstance().currentGame == "metal") addSnow();
			else if (Settings.getInstance().currentGame == "corn") addRain();
			removeHeat();
		}

		public function makeNorm() : void {
			if (heatTw) heatTw.stop();
			heatTw = BetweenAS3.to(heat, {alpha:0}, 2);
			heatTw.play();
			if (coldTw) coldTw.stop();
			coldTw = BetweenAS3.to(cold, {alpha:0}, 2);
			coldTw.play();
			//
			removeSnow();
			removeRain();
			removeHeat();
			if (snd && snd.sounds) snd.playEffect(snd.sounds.forest);
		}

		/* – – – – – – – – – – –  A R T I F A C T S   – – – – – – – – – – – –   */
		private function addSnow() : void {
			if (!snow) {
				snow = new SnowEffect();
				snow.mask = msk;
				addChild(snow);
			}
			if (snd && snd.sounds) snd.playEffect(snd.sounds.blizzard);
		}

		private function addRain() : void {
			if (!rain) {
				rain = new RainEffect();
				rain.mask = msk;
				addChild(rain);
			}
			if (snd && snd.sounds) snd.playEffect(snd.sounds.lightrain);
		}

		private function removeSnow() : void {
			if (snow && this.contains(snow)) {
				this.removeChild(snow);
				snow.mask = null;
				snow = null;
			}
		}

		private function removeRain() : void {
			if (rain && this.contains(rain)) {
				this.removeChild(rain);
				rain.mask = null;
				rain = null;
			}
		}

		private function addHeat() : void {
			if (!air) {
				air = new HeatEffect();
				air.mask = msk;
				addChildAt(air, 0);
			}
		}

		private function removeHeat() : void {
			if (air && this.contains(air)) {
				this.removeChild(air);
				air.mask = null;
				air = null;
			}
		}
	}
}
