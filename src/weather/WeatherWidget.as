package weather {
	import config.Settings;
	import events.WeatherEvent;

	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * @author alexanderdolgov
	 */
	public class WeatherWidget extends Sprite {
		
		public var heat : Sprite;
		public var norm : Sprite;
		public var cold : Sprite;
		public var rainy : Sprite;
		
		public function WeatherWidget() {
			this.mouseChildren = this.mouseEnabled = false;
			heat.visible = cold.visible = rainy.visible = false;
			if (stage) onAddedToStage();
			else addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(event : Event = null) : void {
			if (hasEventListener(Event.ADDED_TO_STAGE)) 
				removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			stage.addEventListener(WeatherEvent.CHANGE, onWeatherChaged);
		}

		private function onWeatherChaged(event : WeatherEvent) : void {
			var wtr : String = String(event.obj);
			var tempr : Sprite = this[wtr] as Sprite;
			if (tempr) setTemp(tempr);
			else trace ("there s no such a weather: " + wtr);
		}

		private function onRemovedFromStage(event : Event) : void {
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			stage.removeEventListener(WeatherEvent.CHANGE, onWeatherChaged);
		}
		
		public function setTemp (temp : Sprite) : void {
			if (temp == cold && Settings.getInstance().currentGame == "corn") temp = rainy;
				
			if (!this.contains(temp)) {
				trace ('I have no such weather!');
				return;
			} 
			heat.visible = cold.visible = norm.visible = rainy.visible = false;
			temp.visible = true;
		}
	}
}
